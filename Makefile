CUDA_HEADERS = -I/usr/include/cuda -I.
CUDART_LIB = -L/usr/lib64 -l cudart
CUDA_LIB = -L/usr/lib64/nvidia -l cuda

objects = cu/ramon_say_hi_definition.o cu/my_kernel.o ramon.o v3.o particle.o main.o   

all: $(objects)
	nvcc $(CUDA_HEADERS) -arch=sm_20 $(objects) -o app

cu/ramon_say_hi_definition.o:
	nvcc $(CUDA_HEADERS) -arch=sm_20 -dc cu/ramon_say_hi_definition.cu -o cu/ramon_say_hi_definition.o

cu/my_kernel.o:
	nvcc $(CUDA_HEADERS) -arch=sm_20 -dc cu/my_kernel.cu -o cu/my_kernel.o

main.o:
	g++ -I. -c main.cpp -o main.o

ramon.o:
	g++ -c ramon.cpp -o ramon.o

v3.o:
	nvcc $(CUDA_HEADERS) -x cu -arch=sm_20 -dc v3.cpp -o v3.o

particle.o:
	nvcc $(CUDA_HEADERS) -x cu -arch=sm_20 -dc particle.cpp -o particle.o

clean:
	rm -f *.o app

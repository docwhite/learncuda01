#include <iostream>
#include "ramon.h"

void ramon_say_hi();

Ramon::Ramon()
{
    std::cout << "Ramon has been created!" << std::endl;
};

void Ramon::say_hi()
{
    ramon_say_hi();
}

Ramon::~Ramon()
{
    std::cout << "Ramon has been destructed!" << std::endl;
}
#ifndef __particle_h__
#define __particle_h__

#ifdef __CUDACC__
#define CUDA_DECORATORS __host__ __device__
#else
#define CUDA_DECORATORS
#endif

#include <v3.h>

class particle
{
    private:
        v3 position;
        v3 velocity;
        v3 totalDistance;

    public:
        particle();
        CUDA_DECORATORS void advance(float dist);
        const v3& getTotalDistance() const;
};

#endif
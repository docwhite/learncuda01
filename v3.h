#ifndef __v3_h__
#define __v3_h__

#ifdef __CUDACC__
#define CUDA_DECORATORS __host__ __device__
#else
#define CUDA_DECORATORS
#endif

class v3
{
public:
    float x;
    float y;
    float z;
    
    v3();
    v3(float _x, float _y, float _z);
    void randomize();    // sets x, y and z to random numbers between 0 and 1
    CUDA_DECORATORS void normalize();
    CUDA_DECORATORS void scramble();

};

#endif